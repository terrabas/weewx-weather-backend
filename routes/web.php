<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $online = false;

    $timestamp = \Carbon\Carbon::now()->startOfDay()->timestamp;
    $current = \App\Archive::orderBy('dateTime','DESC')->first();
    $currentRad = \App\Radiation::orderBy('timestamp','DESC')->first();
    $dayTemps = \App\Archive::where('dateTime','>=', $timestamp)->orderBy('dateTime','DESC')->get();
    $trendTempTime = \Carbon\Carbon::now()->subMinutes(30)->timestamp;
    $trendTemp = \App\Archive::where('dateTime', '<=', $trendTempTime)->orderBy('dateTime','DESC')->first()->outTemp;
    $trendBaro = \App\Archive::where('dateTime', '<=', $trendTempTime)->orderBy('dateTime','DESC')->first()->barometer;

    if($current->dateTime->gt(\Carbon\Carbon::now()->subMinutes('15'))){
        $online = true;
    } else {
        return "No new station data for at least 15 minutes! The station is probably offline.";
        die();
    }

    $trend = round(($current->outTemp - $trendTemp),1);
    //$trend = \App\Helpers\Converter::fahrenheit_to_celsius($trend);

    $maxTemp = $dayTemps->max('outTemp');
    $minTemp = $dayTemps->min('outTemp');
    $averageWindDir = $dayTemps->avg('windDir');

    $minInch = $dayTemps->min('barometer');
    $maxInch = $dayTemps->max('barometer');
    $minBaro = \App\Helpers\Converter::inchesToHpa($minInch);
    $maxBaro = \App\Helpers\Converter::inchesToHpa($maxInch);
    $minBaroTime = $dayTemps->where('barometer',$minInch)->first()->dateTime->format('H:i');
    $maxBaroTime = $dayTemps->where('barometer',$maxInch)->first()->dateTime->format('H:i');

    $baroTrend = round(\App\Helpers\Converter::inchesToHpa($current->barometer - $trendBaro),2);


    $degPerHPA = (100 / 180);
    $BaroDeg = round($degPerHPA * ((\App\Helpers\Converter::inchesToHpa($current->barometer)-950)),1);

    $baroTrendFalling = false;
    if($baroTrend < 0)
        $baroTrendFalling = true;


    $sunrise = \Carbon\Carbon::createFromTimestamp(date_sunrise(time(), SUNFUNCS_RET_TIMESTAMP, 48.342350,15.836450,90, null));
    $sunset = \Carbon\Carbon::createFromTimestamp(date_sunset(time(), SUNFUNCS_RET_TIMESTAMP, 48.342350,15.836450,90, null));

    $daylightTime = $sunrise->diff($sunset);
    $darknessTime = $daylightTime->invert;

    $timeTilPhaseChange = \Carbon\Carbon::now()->diff($sunset);
    $sun_set = true;
    if ($sunrise->isFuture()) {
        $timeTilPhaseChange = Carbon\Carbon::now()->diff($sunrise);
        $sun_set = false;
    }


    return view('welcome', ['current' => $current,
        'online' => $online,
        'maxTemp' => \App\Helpers\Converter::fahrenheit_to_celsius($maxTemp),
        'minTemp' => \App\Helpers\Converter::fahrenheit_to_celsius($minTemp),
        'trend' => $trend,
        'averageWindDir' => $averageWindDir,
        'minBaro' => $minBaro,
        'maxBaro' => $maxBaro,
        'minBaroTime' => $minBaroTime,
        'maxBaroTime' => $maxBaroTime,
        'baroTrend' => $baroTrend,
        'baroDeg' => $BaroDeg,
        'baroTrendFalling' => $baroTrendFalling,
        'currentRad' => $currentRad,
        'sunrise' => $sunrise,
        'sunset' => $sunset,
        'phaseChange' => $timeTilPhaseChange,
        'sun_set' => $sun_set,
        'daylightTime' => $daylightTime,
        'darknessTime' => $darknessTime,
    ]);
});
