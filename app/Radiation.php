<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Radiation extends Model
{
    protected $table = 'radiation';

    public $timestamps = false;

    protected $dates = ['timestamp'];
}
