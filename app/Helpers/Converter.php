<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 03.07.2018
 * Time: 23:19
 */

namespace App\Helpers;


class Converter
{
    public static function fahrenheit_to_celsius($given_value)
    {
        $celsius=5/9*($given_value-32);
        return round($celsius,1) ;
    }

    public static function inchesToHpa($given_value)
    {
        $hpa=33.863886666667*$given_value;
        return round($hpa,2) ;
    }

    public static function mphToKmh($given_value)
    {
        $kmh=1.609*$given_value;
        return round($kmh,1) ;
    }


    public static function degreesToDirection($deg){
        $directions = ['North','Northeast','East','Southeast','South','Southwest','West','Northwest'];
        $deg = round($deg,0);
        $deg=$deg-22.5;
        if($deg <= 0)
            return $directions[0];
        $arr = (int)round(($deg/45),0);
        return $directions[$arr];
    }
}