<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    protected $table = 'archive';

    public $timestamps = false;

    protected $dates = ['dateTime'];
}
