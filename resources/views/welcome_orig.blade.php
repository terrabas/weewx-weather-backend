<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Scripts -->
        <script src="/js/app.js"></script>
        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <script> $(document).foundation();</script>
    </head>
    <body>
    <div class="contain-to-grid sticky">
        <nav class="top-bar" data-topbar role="navigation" data-options="sticky_on: large">
            <center>
            Weather station
            </center>
        </nav>
    </div>

    <div class="row">
        <div class="column medium-6">
            Test
        </div>
        <div class="column medium-6">
            Test2
        </div>
    </div>
    </body>
</html>
