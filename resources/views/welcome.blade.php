<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/main.dark.css" rel="stylesheet">
    <script> $(document).foundation();</script>
</head>
<body>
<div class="weather2-container">
    <div class="container weather34box-toparea">
        <!-- position1 --->
        <div class="weather34box clock">
            <div class="title">
                <svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor stroke-linecap=round
                     stroke-linejoin=round stroke-width=6.25%>
                    <path d="M16 14 L16 23 M16 8 L16 10"/>
                    <circle cx=16 cy=16 r=14/>
                </svg>
                Sunshine
                <orange> Data
            </div>
            <div class="value">
                <div id="position1"></div>
            </div>
        </div>
        <!-- position2--->
        <div class="weather34box indoor">
            <div class="title">
                <svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor stroke-linecap=round
                     stroke-linejoin=round stroke-width=6.25%>
                    <path d="M16 14 L16 23 M16 8 L16 10"/>
                    <circle cx=16 cy=16 r=14/>
                </svg>
                Wind
                <blue>Data</orange>
                    <red>
            </div>
            <div class="value">
                <div id="position2"></div>
            </div>
        </div>
        <!-- position3--->
        <div class="weather34box earthquake">
            <div class="title">
                <svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor stroke-linecap=round
                     stroke-linejoin=round stroke-width=6.25%>
                    <path d="M16 14 L16 23 M16 8 L16 10"/>
                    <circle cx=16 cy=16 r=14/>
                </svg>
                Radiation
                <orange>Data
            </div>
            <div class="value">
                <div id="position3">
                    <div class="hometemperatureindoor">
                        <div class="circlemaxwind" style="width: 50px;height: 50px">{{$currentRad->cpm}}
                            <spanmaxwind>CPM</spanmaxwind>
                            <spanwindtitle> W 100</spanwindtitle>
                        </div>
                        <div class="circlemaxgust" style="width: 50px;height: 50px">{{$currentRad->usvh}}
                            <spanmaxwind>μSv/h</spanmaxwind>
                            <spanwindtitle> W 0.1</spanwindtitle>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- position4--->
        <div class="weather34box alert">
            <div class="title">
                <svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor stroke-linecap=round
                     stroke-linejoin=round stroke-width=6.25%>
                    <path d="M16 14 L16 23 M16 8 L16 10"/>
                    <circle cx=16 cy=16 r=14/>
                </svg>
                Weather
                <red>Alert</red>
            </div>
            <div class="value">
                <div id="position4"></div>
            </div>
        </div>
    </div>
</div>
</div></div>
<!--end position section for homeweatherstation template-->
<!--begin outside/station data section for homeweatherstation template-->
<div class="weather-container">
    <div class="weather-item">
        <div class="chartforecast">
            <span class="yearpopup">  <a href="chartswu/yearlytemperature.php" data-featherlight="iframe"> <svg
                            viewBox="0 0 32 32" width="10" height="10" fill="none" stroke="currentcolor"
                            stroke-linecap="round" stroke-linejoin="round" stroke-width="6.25%"><path
                                d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"></path></svg> 2018 </a></span>
            <span class="monthpopup"> <a href="chartswu/monthlytemperature.php" data-featherlight="iframe"> <svg
                            viewBox="0 0 32 32" width="10" height="10" fill="none" stroke="currentcolor"
                            stroke-linecap="round" stroke-linejoin="round" stroke-width="6.25%"><path
                                d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"></path></svg>  Jul </a></span>
            <span class="todaypopup"> <a href="chartswu/todaytemperature.php" data-featherlight="iframe">  <svg
                            viewBox="0 0 32 32" width="10" height="10" fill="none" stroke="currentcolor"
                            stroke-linecap="round" stroke-linejoin="round" stroke-width="6.25%"><path
                                d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"></path></svg> Today </a></span>
        </div>
        <span class='moduletitle'> Temperature <span class="fgcontrast">&deg;C
</span><br/></span>
        <div id="temperature">
            @if($online)
                <div class="updatedtime"><span><svg id="i-info" viewBox="0 0 32 32" width="7" height="7" fill="#9aba2f"
                                                    stroke="#9aba2f" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="6.25%"><path d="M16 14 L16 23 M16 8 L16 10"></path><circle
                                    cx="16" cy="16" r="14"></circle>
</svg><online> Online </online>
                        @else
                            <div class="updatedtime"><span><svg id="i-info" viewBox="0 0 32 32" width="7" height="7"
                                                                fill="#ff8841" stroke="#ff8841" stroke-linecap="round"
                                                                stroke-linejoin="round" stroke-width="6.25%"><path
                                                d="M16 14 L16 23 M16 8 L16 10"></path><circle cx="16" cy="16"
                                                                                              r="14"></circle>
</svg><offline> Offline </offline>
                                    @endif
                                    {{$current->dateTime->format('H:i:s')}}</span></div>
                            <div class="feelstemp">
                <img src="img/comfortable.svg" width="23px" height="23px">Warm</div>

                            <div class="tempcontainer"><div class="circleOut">
                    <div class="temperaturecircle"></div><div class="temptextwarm">{{\App\Helpers\Converter::fahrenheit_to_celsius($current->outTemp)}}
                                        <suptemp>°</suptemp></div></div><!-- end animated temperature-->
                <div class="max">{{$maxTemp}}°
                    <span style="line-height:0.5em;font-size:12px;vertical-align:middle;"> |</span>
                    {{$minTemp}}°</div>

                <div class="temptrend">
                    -1
                    <trendmovementfalling>&nbsp;<svg id="falling" width="12" height="12" viewBox="0 0 24 24">
                            <polyline points="23 18 13.5 8.5 8.5 13.5 1 6" fill="none" stroke="currentcolor"
                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></polyline>
                            <polyline points="17 18 23 18 23 12" fill="none" stroke="currentcolor"
                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></polyline>
                        </svg> {{$trend}}°</trendmovementfalling>
                </div>
            </div>
                            <div class="heatcircle">
                <div class="heatcircle-content">
                    Feels<br><div class="svgfeels"><img src="css/aqi/feelswarm.svg" width="20px"></div><temporange>{{\App\Helpers\Converter::fahrenheit_to_celsius($current->windchill)}}
                        °C</temporange></div>
                <div class="heatcircle-content">Inside<br>
                    <div class="svgfeels"><img src="css/aqi/feelswarm.svg" width="20px"></div><temporange>{{\App\Helpers\Converter::fahrenheit_to_celsius($current->inTemp)}}
                        °C</temporange></div>
                <div class="heatcircle-content">Humidity <br>
                    <div class="svgfeels"><img src="css/aqi/feelscomfortable.svg" width="20px"></div><tempgreen>Unknown</tempgreen>%

                </div>
                <div class="heatcircle-content">Dewpoint <br><tempblue>
                        <div class="svgfeels"><img src="css/aqi/feelswarm.svg" width="20px"></div><temporange>Unknown°C</temporange></tempblue>
                </div>
            </div>
                </div>


                <br></div>

        <!--forecast for homeweatherstation template-->
        <div class="weather-item">
            <div class="chartforecast">
                <span class="yearpopup"> <a href="outlookds.php" data-featherlight="iframe"><svg viewBox="0 0 32 32"
                                                                                                 width=10 height=10
                                                                                                 fill=none
                                                                                                 stroke=currentcolor
                                                                                                 stroke-linecap=round
                                                                                                 stroke-linejoin=round
                                                                                                 stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> Forecast Summary</a></span>
                <span class="yearpopup">  <a href="forecastdshour.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> Hourly Forecast </a></span>
            </div>
            <span class='moduletitle'>
  Forecast  </span><br/>
            <div id="currentfore"></div>
        </div>
        <!--currentsky for homeweatherstation template-->
        <div class="weather-item">
            <div class="chartforecast">
                <!-- HOURLY & Outlook for homeweather station-->
                <span class="yearpopup"> <a href="metarnearby.php" data-featherlight="iframe"><svg viewBox="0 0 32 32"
                                                                                                   width=10 height=10
                                                                                                   fill=none
                                                                                                   stroke=currentcolor
                                                                                                   stroke-linecap=round
                                                                                                   stroke-linejoin=round
                                                                                                   stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> Nearby Metar</a></span>

            </div>
            <span class='moduletitle'>Current Conditions</span><br/>
            <div id="currentsky"></div>
        </div>
    </div>
    <!--windspeed for homeweatherstation template-->
    <div class="weather-container">
        <div class="weather-item">
            <div class="chartforecast">
                <span class="yearpopup">  <a href="chartswu/yearlywindspeedgust.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> 2018</a></span>
                <span class="monthpopup"> <a href="chartswu/monthlywindspeedgust.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg>  Jul </a></span>
                <span class="todaypopup"> <a href="chartswu/todaywindspeedgust.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> Today </a></span>
            </div>
            <span class='moduletitle'>Wind | Gust</span><br/>
            <div id="windspeed">
                <style>.thearrow2 {
                        -webkit-transform: rotate({{round($current->windDir,0)}}deg);
                        -moz-transform: rotate({{round($current->windDir,0)}}deg);
                        -o-transform: rotate({{round($current->windDir,0)}}deg);
                        -ms-transform: rotate({{round($current->windDir,0)}}deg);
                        transform: rotate({{round($current->windDir,0)}}deg);
                        position: absolute;
                        z-index: 200;
                        top: 0;
                        left: 50%;
                        margin-left: -5px;
                        width: 10px;
                        height: 50%;
                        -webkit-transform-origin: 50% 100%;
                        -moz-transform-origin: 50% 100%;
                        -o-transform-origin: 50% 100%;
                        -ms-transform-origin: 50% 100%;
                        transform-origin: 50% 100%;
                        -webkit-transition-duration: 3s;
                        -moz-transition-duration: 3s;
                        -o-transition-duration: 3s;
                        -ms-transition-duration: 3s;
                        transition-duration: 3s
                    }

                    .thearrow2:after {
                        content: '';
                        position: absolute;
                        left: 50%;
                        top: 0;
                        height: 10px;
                        width: 10px;
                        background-color: NONE;
                        width: 0;
                        height: 0;
                        border-style: solid;
                        border-width: 14px 9px 0 9px;
                        border-color: RGBA(255, 121, 58, 1.00) transparent transparent transparent;
                        -webkit-transform: translate(-50%, -50%);
                        -moz-transform: translate(-50%, -50%);
                        -o-transform: translate(-50%, -50%);
                        -ms-transform: translate(-50%, -50%);
                        transform: translate(-50%, -50%);
                        -webkit-transition-duration: 3s;
                        -moz-transition-duration: 3s;
                        -o-transition-duration: 3s;
                        -ms-transition-duration: 3s;
                        transition-duration: 3s
                    }

                    .thearrow2:before {
                        content: '';
                        width: 6px;
                        height: 6px;
                        position: absolute;
                        z-index: 9;
                        left: 2px;
                        top: -5px;
                        border: 2px solid RGBA(255, 255, 255, 0.8);
                        -webkit-border-radius: 100%;
                        -moz-border-radius: 100%;
                        -o-border-radius: 100%;
                        -ms-border-radius: 100%;
                        border-radius: 100%
                    }

                    .avgw {
                        width: 24px;
                        height: 24px;
                        margin-left: 37px;
                        -webkit-transform: rotate({{round($averageWindDir,0)}}deg);
                        -moz-transform: rotate({{round($averageWindDir,0)}}deg);
                        -o-transform: rotate({{round($averageWindDir,0)}}deg);
                        -ms-transform: rotate({{round($averageWindDir,0)}}deg);
                        transform: rotate({{round($averageWindDir,0)}}deg);
                    }

                    spancalm {
                        postion: relative;
                        font-family: weathertext, Arial;
                        font-size: 20px;
                    }</style>
                <div class="updatedtime">
<span>
                    @if($online)
        <svg id="i-info" viewBox="0 0 32 32" width="7" height="7" fill="#9aba2f" stroke="#9aba2f" stroke-linecap="round"
             stroke-linejoin="round" stroke-width="6.25%"><path d="M16 14 L16 23 M16 8 L16 10"></path><circle cx="16"
                                                                                                              cy="16"
                                                                                                              r="14"></circle>
</svg></span>
                    @else
                        <svg id="i-info" viewBox="0 0 32 32" width="7" height="7" fill="#ff8841" stroke="#ff8841"
                             stroke-linecap="round" stroke-linejoin="round" stroke-width="6.25%">
                            <path d="M16 14 L16 23 M16 8 L16 10"></path>
                            <circle cx="16" cy="16" r="14"></circle>
                        </svg></span> @endif{{$current->dateTime->format('H:i:s')}}</div>
            </div>
            <div class="averagedir1">
                <div class="avgw"><img src="css/windicons/avgw.svg" width="24px" height="24px"></div>
                <ogrey> Avg</ogrey>
                <ogreen> NA°</ogreen>
                <oorange> NA</oorange>
                <ogrey>km/h</ogrey>
            </div>
            <div class="windrun">
                <windrun> Wind Run:</windrun>
                <span>NA km</span>
            </div>
            <br>
            <div class="windicons1">
                {{ \App\Helpers\Converter::mphToKmh($current->windSpeed) }}
                <img src="css/windspeed/wind2kts.svg"></div> <!-- end wind speed icons -->
            <div class="beaufort1">
                &nbsp;Light Air
            </div>
            <div class="windspeedvalues">
                <div class="windspeedvalue">
                    {{\App\Helpers\Converter::mphToKmh($current->windSpeed)}}
                    <div class="windunitidspeed">
                        km/h
                    </div>
                </div>
                <div class="windgustvalue">
                    {{ \App\Helpers\Converter::mphToKmh($current->windGust) }}
                    <div class="windunitidgust">
                        km/h
                    </div>
                </div>
            </div>

            <div class="windspeedtrend1">
                <supmb>Max</supmb>
                <max>NA</max>
                <supmb> km/h</supmb>
                <br> Wind (<b>00:09</b>)
            </div>
            <div class="gustspeedtrend1">
                <supmb>Max</supmb>
                <max>NA</max>
                <supmb> km/h</supmb>
                <br> Gust (<b>00:32</b>)
            </div>
            <div class="homeweathercompass1">
                <div class="homeweathercompass-line1">
                    <div class="thearrow2"></div>
                </div>
                <div class="text1">
                    <div class="windvalue1"
                         id="windvalue">{{($current->windSpeed == 0)?'No wind':round($current->windDir,0)."°"}}</div>
                </div>
                <div class="windirectiontext1">
                    <!-- Due <span>North<br></span> </div> -->
                    @if($current->windSpeed != 0)
                        Due <span>{{\App\Helpers\Converter::degreesToDirection($current->windDir)}}<br></span>
                    @endif
                </div>
            </div>
        </div>
        <!--barometer for homeweatherstation template-->
        <div class="weather-item">
            <div class="chartforecast" style="z-index:20">
                <span class="yearpopup">  <a href="chartswu/yearlybarometer.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> 2018 </a></span>
                <span class="monthpopup"> <a href="chartswu/monthlybarometer.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg>  Jul </a></span>
                <span class="todaypopup"> <a href="chartswu/todaybarometer.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> Today</a></span>
            </div>
            <span class='moduletitle'>Barometer   </span><br/>
            <div id="barometer">
                <meta http-equiv="Content-Type: text/html; charset=UTF-8">
                <style>
                    .thearrow3 {
                        -webkit-transform: rotate({{$baroDeg}}deg);
                        -moz-transform: rotate({{$baroDeg}}deg);
                        -o-transform: rotate({{$baroDeg}}deg);
                        -ms-transform: rotate({{$baroDeg}}deg);
                        transform: rotate({{$baroDeg}}deg);
                        position: absolute;
                        z-index: 200;
                        top: 0;
                        left: 50%;
                        margin-left: -5px;
                        width: 10px;
                        height: 50%;
                        -webkit-transform-origin: 50% 100%;
                        -moz-transform-origin: 50% 100%;
                        -o-transform-origin: 50% 100%;
                        -ms-transform-origin: 50% 100%;
                        transform-origin: 50% 100%;
                        -webkit-transition-duration: 3s;
                        -moz-transition-duration: 3s;
                        -o-transition-duration: 3s;
                        -ms-transition-duration: 3s;
                        transition-duration: 3s
                    }
                </style>
                <div class="updatedtime"><span>
                    @if($online)
                            <svg id="i-info" viewBox="0 0 32 32" width="7" height="7" fill="#9aba2f" stroke="#9aba2f"
                                 stroke-linecap="round" stroke-linejoin="round" stroke-width="6.25%"><path
                                        d="M16 14 L16 23 M16 8 L16 10"></path><circle cx="16" cy="16" r="14"></circle>
</svg></span>
                    @else
                        <svg id="i-info" viewBox="0 0 32 32" width="7" height="7" fill="#ff8841" stroke="#ff8841"
                             stroke-linecap="round" stroke-linejoin="round" stroke-width="6.25%">
                            <path d="M16 14 L16 23 M16 8 L16 10"></path>
                            <circle cx="16" cy="16" r="14"></circle>
                        </svg></span> @endif{{$current->dateTime->format('H:i:s')}}</div>
                <div class="barometermax">
                    <div class="barometerorange">Max (<b>{{$maxBaroTime}}</b>)<br><span>{{$maxBaro}}&nbsp;hPa</span>
                    </div>
                </div>
                <div class="barometermin">
                    <div class="barometerblue">Min (<b>{{$minBaroTime}}</b>)<br><span>{{$minBaro}}&nbsp;hPa</span></div>
                </div>
                <div class="barometertrend2">
                    trend
                    <rising>
                        <rise>
                            <svg id="rising" width="12" height="12" viewBox="0 0 24 24">
                                <polyline points="23 6 13.5 15.5 8.5 10.5 1 18" fill="none" stroke="currentcolor"
                                          stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></polyline>
                                <polyline points="17 6 23 6 23 12" fill="none" stroke="currentcolor"
                                          stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></polyline>
                            </svg>
                        </rise>
                        <br><span> {{$baroTrend}}</span></rising>
                    <units> hPa</units>
                </div>
                <div class="barometerconv">
                    <convtext>inches:</convtext> {{round($current->barometer,2)}}&nbsp;<span>in</span></div>
                <div class="homeweathercompass2">
                    <div class="homeweathercompass-line2">
                        <div class="thearrow3"></div>
                    </div>
                    <div class="text2">
                        <div class="pressuretext">
                            @if($baroTrendFalling)
                                <blue>
                                    falling
                                    <fall>
                                        <svg id="falling" width="12" height="12" viewBox="0 0 24 24">
                                            <polyline points="23 18 13.5 8.5 8.5 13.5 1 6" fill="none"
                                                      stroke="currentcolor" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="2"></polyline>
                                            <polyline points="17 18 23 18 23 12" fill="none" stroke="currentcolor"
                                                      stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-width="2"></polyline>
                                        </svg>
                                    </fall>
                                </blue>
                            @else
                                <oorange>
                                    rising
                                    <rise>
                                        <svg id="rising" width="12" height="12" viewBox="0 0 24 24">
                                            <polyline points="23 6 13.5 15.5 8.5 10.5 1 18" fill="none"
                                                      stroke="currentcolor" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="2"></polyline>
                                            <polyline points="17 6 23 6 23 12" fill="none" stroke="currentcolor"
                                                      stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-width="2"></polyline>
                                        </svg>
                                    </rise>
                                </oorange>
                            @endif </div>{{\App\Helpers\Converter::inchesToHpa($current->barometer)}}
                        &nbsp;<span>hPa</span></div>
                </div>
                <div class="barometerlimits">
                    <div class="barometerlimits">950 ---------------------------------- 1050</div>
                </div>
            </div>
        </div>
        <!--moonphase for homeweatherstation template-->

        <div class="weather-item">
            <div class="chartforecast" style="margin-top:172px;z-index:20;">
                <div class="moonposition1">
                    <div class="wi wi-moon-5"></div>
                    <span style="color:#777;font-weight:normal;font-size:13px">  </span>
                </div>
                <span class="yearpopup" style="margin-top:-5px;z-index:20;"><a href="mooninfo.php"
                                                                               data-featherlight="iframe">
First Quarter</a></span>
                <span class="monthpopup"><a href="meteorshowers.php" data-featherlight="iframe"><svg width="12px"
                                                                                                     height="12px"
                                                                                                     viewBox="0 0 16 16"><path
                                    fill="currentcolor"
                                    d="M0 0l14.527 13.615s.274.382-.081.764c-.355.382-.82.055-.82.055L0 0zm4.315 1.364l11.277 10.368s.274.382-.081.764c-.355.382-.82.055-.82.055L4.315 1.364zm-3.032 2.92l11.278 10.368s.273.382-.082.764c-.355.382-.819.054-.819.054L1.283 4.284zm6.679-1.747l7.88 7.244s.19.267-.058.534-.572.038-.572.038l-7.25-7.816zm-5.68 5.13l7.88 7.244s.19.266-.058.533-.572.038-.572.038l-7.25-7.815zm9.406-3.438l3.597 3.285s.094.125-.029.25c-.122.125-.283.018-.283.018L11.688 4.23zm-7.592 7.04l3.597 3.285s.095.125-.028.25-.283.018-.283.018l-3.286-3.553z"></path></svg> &nbsp;Meteor Showers</a></span>
                <span class="todaypopup"><a href="aurora.php" data-featherlight="iframe"><svg id="i-info"
                                                                                              viewBox="0 0 32 32"
                                                                                              width="10" height="10"
                                                                                              fill="none"
                                                                                              stroke="currentcolor"
                                                                                              stroke-linecap="round"
                                                                                              stroke-linejoin="round"
                                                                                              stroke-width="6.25%"><path
                                    d="M16 14 L16 23 M16 8 L16 10"></path><circle cx="16" cy="16" r="14"></circle></svg>  Aurora </a></span>
            </div>
            <span class="moduletitle">Daylight</span><br>
            <div id="moonphase">


                <meta http-equiv="Content-Type: text/html; charset=UTF-8">
                <style>
                    .daylightvalue1:before {
                        position: absolute;
                        content: "Estimated";
                        display: block;
                        font-size: 11px;
                        line-height: 20px;
                        top: -28px;
                        left: 45px;
                        letter-spacing: normal;
                        border: 0;
                    }

                    .thearrow5 {
                        -webkit-transform: rotate(
                                124.15deg);
                        -moz-transform: rotate(
                                124.15deg);
                        -o-transform: rotate(
                                124.15deg);
                        -ms-transform: rotate(
                                124.15deg);

                        transform: rotate(
                                124.15deg);
                        position: absolute;
                        z-index: 200;
                        top: 0;
                        left: 44%;
                        margin-left: -4px;
                        width: 10px;
                        height: 50%;
                        -webkit-transform-origin: 50% 100%;
                        -moz-transform-origin: 50% 100%;
                        -o-transform-origin: 50% 100%;
                        -ms-transform-origin: 50% 100%;
                        transform-origin: 50% 100%;
                    }

                    .thearrow5:before {
                        color: #ff8841;
                        display:;
                    }

                </style>
                <div class="daylightmoduleposition">
                    <div class="sunlightday">
                        <svg version="1.1" id="weather34 daylight" x="0px" y="0px" width="12" height="12" fill="#ff8841"
                             viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
<g>
    <path fill="#ff8841"
          d="M270.3,500c0,126.9,102.8,229.7,229.7,229.7S729.7,626.9,729.7,500c0-126.9-102.8-229.7-229.7-229.7S270.3,373.1,270.3,500z"></path>
    <path d="M500,193.8c16.8,0,30.6-13.8,30.6-30.6V40.6c0-16.8-13.8-30.6-30.6-30.6c-16.8,0-30.6,13.8-30.6,30.6v122.5C469.4,180,483.2,193.8,500,193.8z"></path>
    <path d="M500,806.3c-16.8,0-30.6,13.8-30.6,30.6v122.5c0,16.8,13.8,30.6,30.6,30.6c16.8,0,30.6-13.8,30.6-30.6V836.9C530.6,820,516.8,806.3,500,806.3z"></path>
    <path d="M959.4,469.4H836.9c-16.8,0-30.6,13.8-30.6,30.6c0,16.8,13.8,30.6,30.6,30.6h122.5c16.8,0,30.6-13.8,30.6-30.6C990,483.2,976.2,469.4,959.4,469.4z"></path>
    <path d="M193.8,500c0-16.8-13.8-30.6-30.6-30.6H40.6C23.8,469.4,10,483.2,10,500c0,16.8,13.8,30.6,30.6,30.6h122.5C180,530.6,193.8,516.8,193.8,500z"></path>
    <path d="M239.7,284.1c6.1,6.1,13.8,9.2,21.4,9.2s15.3-3.1,21.4-9.2c12.3-12.3,12.3-30.6,0-42.9l-87.3-87.3c-12.3-12.3-30.6-12.3-42.9,0s-12.3,30.6,0,42.9L239.7,284.1z"></path>
    <path d="M760.3,715.9c-12.3-12.3-30.6-12.3-42.9,0s-12.3,30.6,0,42.9l87.3,87.3c6.1,6.1,13.8,9.2,21.4,9.2s15.3-3.1,21.4-9.2c12.3-12.3,12.3-30.6,0-42.9L760.3,715.9z"></path>
    <path d="M738.9,291.8c7.7,0,15.3-3.1,21.4-9.2l87.3-87.3c12.3-12.3,12.3-30.6,0-42.9s-30.6-12.3-42.9,0l-88.8,87.3c-12.3,12.3-12.3,30.6,0,42.9C722,288.7,729.7,291.8,738.9,291.8z"></path>
    <path d="M239.7,715.9l-87.3,87.3c-12.3,12.3-12.3,30.6,0,42.9c6.1,6.1,13.8,9.2,21.4,9.2s15.3-3.1,21.4-9.2l87.3-87.3c12.3-12.3,12.3-30.6,0-42.9C271.8,705.2,251.9,705.2,239.7,715.9z"></path>
</g>
</svg>
                        <strongnumbers>{{$daylightTime->format('H:i')}}</strongnumbers>
                        hrs <br>
                        <period>Total Daylight</period>
                    </div>

                    <div class="sundarkday">
                        <svg id="i-ban" viewBox="0 0 32 32" width="8" height="8" fill="#777" stroke="#777"
                             stroke-linecap="round" stroke-linejoin="round" stroke-width="6.25%">
                            <circle cx="16" cy="16" r="14"></circle>
                            <path d="M6 6 L26 26"></path>
                        </svg>
                        <strongnumbers>{{$darknessTime}}:
                            <minutes>{{$darknessTime}}</minutes>
                        </strongnumbers>
                        hrs <br>
                        <period>Total Darkness</period>
                    </div>


                    <div class="sunriseday"> Sun Rise
                        <svg id="weather34 sunrise" width="10px" height="10px" x="0px" y="0px" viewBox="0 0 64 64"
                             style="enable-background:new 0 0 64 64;">
                            <g>
                                <path fill="#ff8841"
                                      d="M32,2.8c-16,0-29.1,13-29.2,29h58.4C61.1,15.8,48,2.8,32,2.8z"></path>
                                <path class="st1" d="M32,2C15.5,2,2,15.5,2,32s13.5,30,30,30c16.5,0,30-13.5,30-30S48.5,2,32,2z M61.6,32
c0,16.4-13.3,29.6-29.6,29.6C15.6,61.6,2.4,48.4,2.4,32c0-0.1,0-0.2,0-0.2C2.5,15.5,15.7,2.4,32,2.4c16.3,0,29.5,13.1,29.6,29.4 C61.6,31.8,61.6,31.9,61.6,32z"></path>
                            </g>
                        </svg>
                        <br>
                        &nbsp;&nbsp;
                        Tomorrow
                        <strongnumbers>{{$sunrise->toTimeString()}} </strongnumbers>
                    </div>


                    <div class="sunsetday">
                        <svg id="weather34 sunset" width="10px" height="10px" x="0px" y="0px" viewBox="0 0 64 64"
                             style="enable-background:new 0 0 64 64;">
                            <g>
                                <path fill="#d86858"
                                      d="M32,61.2c16.1,0,29.2-13.1,29.2-29.2c0-0.1,0-0.2,0-0.2H2.8c0,0.1,0,0.2,0,0.2C2.8,48.1,15.9,61.2,32,61.2z"></path>
                                <path class="st1" d="M32,2C15.5,2,2,15.5,2,32s13.5,30,30,30c16.5,0,30-13.5,30-30S48.5,2,32,2z M61.6,32
c0,16.4-13.3,29.6-29.6,29.6C15.6,61.6,2.4,48.4,2.4,32c0-0.1,0-0.2,0-0.2C2.5,15.5,15.7,2.4,32,2.4c16.3,0,29.5,13.1,29.6,29.4	C61.6,31.8,61.6,31.9,61.6,32z"></path>
                            </g>
                        </svg>
                        Sun Set <br>
                        Today
                        <strongnumbers> {{$sunset->toTimeString()}}</strongnumbers>
                    </div>


                    <div class="daylightcompass2">
                        <div class="daylightcompass-line2">
                            <div class="thearrow5"></div>
                        </div>
                        <div class="daylightvalue1">
                            <hrs>hrs</hrs>
                            <span><svg id="i-clock" viewBox="0 0 32 32" width="5" height="5" fill="none"
                                       stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round"
                                       stroke-width="10%">
<circle cx="16" cy="16" r="14"></circle>
<path d="M16 8 L16 16 20 20"></path>
</svg></span>
                            {{$phaseChange->format('H')}}
                            <minutes>{{$phaseChange->format('i')}}</minutes>
                            <br>
                            <period>Till Sun {{($sun_set)?'Set':'Rise'}}</period>
                            <min>min</min>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--rainfall for homeweatherstation template-->
    <div class="weather-container">
        <div class="weather-item">
            <div class="chartforecast">
                <span class="yearpopup">  <a href="chartswu/yearlyrainfall.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> 2018 </a></span>
                <span class="monthpopup"> <a href="chartswu/monthlyrainfall.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg>  Jul </a></span>
                <span class="todaypopup"> <a href="chartswu/todayrainfall.php" data-featherlight="iframe"><svg
                                viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor
                                stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path
                                    d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> Today </a></span>
            </div>
            <span class='moduletitle'>Rainfall Today</span><br/>
            <div id="rainfall"></div>
        </div>
        <!--solar or web cam for homeweatherstation template-->
        <div class="weather-item">
            <div class="chartforecast">
  <span class="yearpopup">
  </a></span>
                <span class="monthpopup">
     <a href="uvindex.php" data-featherlight="iframe"> <svg viewBox="0 0 32 32" width=10 height=10 fill=none
                                                            stroke=currentcolor stroke-linecap=round
                                                            stroke-linejoin=round stroke-width=6.25%><path
                     d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18"/></svg> UV - Solar Info </a></span>


            </div>
            <span class='moduletitle'>
  UVI-Solar</span></span><br/>
            <div id="solar"></div>
        </div>
        <!--air quality- moon for homeweatherstation template-->
        <div class="weather-item">
            <div class="chartforecast" style="z-index:20">
  <span class="monthpopup">
   <span class="yearpopup"> <a href="eqlist.php" data-featherlight="iframe"> <svg id=i-activity viewBox="0 0 32 32"
                                                                                  width=10 height=10 fill=none
                                                                                  stroke=currentcolor
                                                                                  stroke-linecap=round
                                                                                  stroke-linejoin=round
                                                                                  stroke-width=6.25%><path
                       d="M4 16 L11 16 14 29 18 3 21 16 28 16"/>
</svg>  Earthquake   </a></span>
            </div>
            <span class='moduletitle'>
	 Indoor     </span></span>
            <div id="dldata"></div>
        </div>
    </div>
    <!--end outdoor data for homeweatherstation template-->
    <!--footer area for homeweatherstation template warning dont mess with this below this line unless you really know what you are doing-->
    <div class="weatherfooter-container">
        <div class="weatherfooter-item">
            <!--<div class="cclicencelogo"><a href="http://sandaysoft.com/products/cumulus" target="_blank" title="http://sandaysoft.com/products/cumulus">
                    <img src="img/cumulus.svg" width"25px" height="25px"></a>
            </div> -->
            <div class="designedby">
                original design by <a href="https://weather34.com" title="weather34.com ">weather34.com </a></div>
            <div class="footertext">
                <svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor stroke-linecap=round
                     stroke-linejoin=round stroke-width=6.25%>
                    <path d="M16 14 L16 23 M16 8 L16 10"/>
                    <circle cx=16 cy=16 r=14/>
                </svg> &nbsp; Data Source: schwarzis.net weather server
                (1098)&nbsp;
                <svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor stroke-linecap=round
                     stroke-linejoin=round stroke-width=6.25%>
                    <path d="M16 14 L16 23 M16 8 L16 10"/>
                    <circle cx=16 cy=16 r=14/>
                </svg> &nbsp; Hardware: Peetbros Ultimeter 2100
            </div>
            <div class="footertext">&nbsp; &nbsp;<svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none
                                                      stroke=currentcolor stroke-linecap=round stroke-linejoin=round
                                                      stroke-width=6.25%>
                    <path d="M16 14 L16 23 M16 8 L16 10"/>
                    <circle cx=16 cy=16 r=14/>
                </svg> &nbsp;Operational Since:&nbsp; Jan 2018
            </div>
            <!--<div class="footertext"><svg id=i-info viewBox="0 0 32 32" width=10 height=10 fill=none stroke=currentcolor stroke-linecap=round stroke-linejoin=round stroke-width=6.25%><path d="M16 14 L16 23 M16 8 L16 10" /><circle cx=16 cy=16 r=14 /></svg> &nbsp; You can include a personalised footer description.. try and keep it short doesn't need to be long.&nbsp;<img rel="prefetch" src="img/flags/en.svg"width="23px" height="23px"> </div>
        -->
        </div>
    </div>


    <header>
        <h1>
            <ogreyh1>Oberbierbaum&nbsp;</ogreyh1>
            <ogreenh1>OE3SMB</ogreenh1>
            <ogreyh1>&#8226; Ultimeter 2100 &#8226;</ogreyh1>
            <oblueh1> WEATHER</oblueh1>
        </h1>
        <div class="menuclock">
            <div class="clock-container">
                <memclock>
                    <svg id="menu clock" viewBox="0 0 32 32" width="12" height="12" fill="none" stroke="currentcolor"
                         stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                        <circle cx="16" cy="16" r="14"></circle>
                        <path d="M16 8 L16 16 20 20"></path>
                    </svg>
                </memclock>
                <ul style="line-height: 0">
                    <li style="margin-left: 10px"><span>{{\Carbon\Carbon::now()->format('H:i:s')}}</span></li>
                </ul>
            </div>
        </div>
    </header>
</body>
</html>
